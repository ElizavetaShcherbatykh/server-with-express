const express = require('express');
const pjson = require('./package.json')
const app = express();

const OPERATION_NAME = {
    MULTIPLICATION: 'multiplication',
    SUMMATION: 'summation',
};

const operationsHistory = [];


class TimeOfResponse {
    constructor() {
        this.startTime = process.hrtime();
        this.NS_PER_SEC = 1e9;
        this.NS_TO_MS = 1e6;
    }
    getTimeOfResponse() {
        const diff = process.hrtime(this.startTime);
        const time = (diff[0] * this.NS_PER_SEC + diff[1]) / this.NS_TO_MS;
        return time;
    }
}

class OperationName {
    addOperationToHistory(finalHistoryData) {
        if (finalHistoryData.operationName === OPERATION_NAME.MULTIPLICATION || finalHistoryData.operationName === OPERATION_NAME.SUMMATION) {
            const createdAt = Date.now();
            operationsHistory.push({...finalHistoryData, createdAt})
        }
        return
    }
}


app.use((req, res, next) => {
    req.timeHelper = new TimeOfResponse();
    req.operationHendler = new OperationName();
    next();
});

app.get('/', async (req, res) => {
    res.send('Hello API');
})

app.get('/information', async (req, res) => {
    const data = {
        version: pjson.version.split('').reverse().join(''),
        uptime: process.uptime(),
    };
    const timeOfResponse = req.timeHelper.getTimeOfResponse();
    const finalData = { ...data, timeOfResponse };
    res.send(finalData);
})

app.get(`/operation/${OPERATION_NAME.SUMMATION}`, async (req, res) => {
    const { firstOperand, secondOperand } = req.query;
    const data = {
        firstOperand: +firstOperand,
        secondOperand: +secondOperand,
        total: +firstOperand + +secondOperand,
    }
    const operationName = OPERATION_NAME.SUMMATION;
    const timeOfResponse = req.timeHelper.getTimeOfResponse();
    const finalData = { ...data, timeOfResponse };
    req.operationHendler.addOperationToHistory({...finalData, operationName})
    res.send(finalData);
})

app.get(`/operation/last`, async (req, res) => {
    const sortedOperationsArray = [...operationsHistory].sort((a,b) => a.createdAt - b.createdAt);
    const indxOfLast = sortedOperationsArray.length-1;
    const lastOperationName = sortedOperationsArray[indxOfLast].operationName;
    const data = {
        lastOperationName,
    }
    const timeOfResponse = req.timeHelper.getTimeOfResponse();
    const finalData = { ...data, timeOfResponse };
    res.send(finalData);
})

app.get(`/operation/${OPERATION_NAME.MULTIPLICATION}`, async (req, res) => {
    const { firstOperand, secondOperand } = req.query;
    const data = {
        firstOperand: +firstOperand,
        secondOperand: +secondOperand,
        total: +firstOperand * +secondOperand,
    }
    const operationName = OPERATION_NAME.MULTIPLICATION;
    const timeOfResponse = req.timeHelper.getTimeOfResponse();
    const finalData = { ...data, timeOfResponse };
    req.operationHendler.addOperationToHistory({...finalData, operationName})
    res.send(finalData);
})

app.get('/operation/information', async (req, res) => {
    const { totalAmountOfOperation, summation, multiplication } = operationCounter;
    const totalAmountOfOperation = operationsHistory.length;
    const multiplication = operationsHistory.filter(item => item.operationName === OPERATION_NAME.MULTIPLICATION).length;
    const summation = operationsHistory.filter(item => item.operationName === OPERATION_NAME.SUMMATION).length;
    const data = {
        totalAmountOfOperation,
        multiplication,
        summation,
    }
    const timeOfResponse = req.timeHelper.getTimeOfResponse();
    const finalData = { ...data, timeOfResponse };
    res.send(finalData);
})

app.get('/operation/history', async (req, res) => {
    const { sort } = req.query;
    const testOperationsHistory = operationsHistory.map(({ createdAt, ...others }) => ({ ...others }));
    const history = [...testOperationsHistory ].reverse();
    const data = {
        history: sort === 'DESC' ? history : testOperationsHistory,
    }
    const timeOfResponse = req.timeHelper.getTimeOfResponse();
    const finalData = { ...data, timeOfResponse };
    res.send(finalData);
})

app.listen(3000, () => console.log('API server on'))